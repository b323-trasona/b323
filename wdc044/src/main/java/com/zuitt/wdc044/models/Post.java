package com.zuitt.wdc044.models;

//Java Persistence A
import javax.persistence.*;
//mark This java object as a representation of a database Table via @Entity
@Entity
//designate table via Table
@Table(name = "posts")
public class Post {
    //Indicate that this property represents the primary key via @Id
    @Id
    //value for this will be auto incremented
    @GeneratedValue
    private Long id;

    //class Property that represent table columns in  a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name="user_id",nullable = false)
    private User user;


    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content =content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }
}
