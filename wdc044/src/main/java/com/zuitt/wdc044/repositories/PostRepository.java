package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//an interface contains behavior that a class implements
//an interface marked as @Repository contains methods for databse manipulation
//by extending CrudRepository, PostRepository has inhereted ite predefined methods for creating retrieving
public interface PostRepository extends CrudRepository<Post,Object> {
}
